from django.conf.urls import url
from django.urls import path

from dairy import views
from dairy.views import DetailsAPIView

app_name = "dairy"

urlpatterns = [
    path('', views.index, name='index'),
    url('api/v1/details', DetailsAPIView.as_view(), name='details'),
]
