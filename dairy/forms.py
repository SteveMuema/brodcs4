from django import forms


class DateInput(forms.DateInput):
    input_type = 'date'


class InputForm(forms.Form):
    date = forms.DateTimeField(
        widget=DateInput()
    )
    prev_sales = forms.IntegerField()
    sales = forms.IntegerField()
