from pathlib import Path

from flask import render_template
import sqlite3
import requests
from flask import Flask
import joblib
import pandas as pd
import os
import numpy as np
import json

from rest_framework import serializers
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from tensorflow.keras.models import load_model
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, Flatten, Dense
from sklearn.preprocessing import MinMaxScaler

BASE_DIR = Path(__file__).resolve().parent.parent

model = load_model("{}/files/my_model.h5".format(BASE_DIR))
model.summary()

from itertools import islice
from django.shortcuts import render

# Create your views here.
from dairy.forms import InputForm


class DetailsSerializer(serializers.Serializer):
    date = serializers.DateTimeField(format='%Y-%m-%d', input_formats=['%Y-%m-%d'])
    prev_sales = serializers.IntegerField()
    sales = serializers.IntegerField()


class DetailsAPIView(GenericAPIView):
    serializer_class = DetailsSerializer
    permission_classes = ()

    def get(self, request):
        # yourdata = [{"date": 10, "comments": 0}, {"likes": 4, "comments": 23}]
        # results = DetailsSerializer(yourdata, many=True).data
        # return Response(results)
        pass

    def post(self, request, format=None):
        print(request.data['date']+ " is the date")
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        prediction = predict(request.data['date'],prev_sales=request.data['prev_sales'],Sales=request.data['sales'])
        data = prediction[0]
        print(data)
        return Response(data)


def slice_data_for_lstm(data, lookback):
    return np.array(
        list(zip(*[islice(np.array(data), i, None, 1) for i in range(lookback)]))
    )


def predict(date, prev_sales, Sales):
    date = str(date)
    prev_sales = float(prev_sales)
    Sales = float(Sales)
    diff = Sales - prev_sales

    print(date, prev_sales)

    # dictionary of lists
    dict = {
        "Week Ending Date": date,
        "Sales": Sales,
        "prev_sales": prev_sales,
        "diff": diff,
    }

    df = pd.DataFrame(dict, index=[0])
    print(df)

    new_dairy = df[["Week Ending Date", "Sales"]]

    df_supervised = df.drop(["prev_sales"], axis=1)
    print(df_supervised)

    # adding lags
    for inc in range(1, 13):
        field_name = "lag_" + str(inc)
        df_supervised[field_name] = df_supervised["diff"].shift(inc)

    # df_supervised = df_supervised.dropna().reset_index(drop=True)
    df_supervised = df_supervised.fillna(0)

    print(df_supervised)

    df_model = df_supervised.drop(["Sales", "Week Ending Date"], axis=1)

    test_set = df_model.values
    scaler = MinMaxScaler(feature_range=(-1, 1))

    scaler = scaler.fit(test_set)
    # reshape test set
    test_set = test_set.reshape(test_set.shape[0], test_set.shape[1])
    test_set_scaled = scaler.transform(test_set)

    test_set_scaled = scaler.transform(test_set)
    X_test, y_test = test_set_scaled[:, 1:], test_set_scaled[:, 0:1]
    X_test = X_test.reshape(X_test.shape[0], 1, X_test.shape[1])

    print(test_set)
    print(X_test)
    print(X_test.shape)

    y_pred = model.predict(X_test)
    print(y_pred)
    y_pred = y_pred.reshape(y_pred.shape[0], 1, y_pred.shape[1])

    pred_test_set = []
    for index in range(0, len(y_pred)):
        print(np.concatenate([y_pred[index], X_test[index]], axis=1))
        pred_test_set.append(np.concatenate([y_pred[index], X_test[index]], axis=1))

    # reshape pred_test_set
    pred_test_set = np.array(pred_test_set)
    pred_test_set = pred_test_set.reshape(
        pred_test_set.shape[0], pred_test_set.shape[2]
    )

    # inverse transform
    pred_test_set_inverted = scaler.inverse_transform(pred_test_set)
    print(pred_test_set_inverted)

    df_diff = new_dairy.copy()
    new_dairy.rename(columns={"Week Ending Date": "Date"}, inplace=True)

    # create dataframe that shows the predicted sales
    result_list = []
    sales_dates = list(new_dairy.Date)
    act_sales = list(new_dairy.Sales)

    for index in range(0, len(pred_test_set_inverted)):
        result_dict = {}
        result_dict["pred_value"] = int(
            pred_test_set_inverted[index][0] + act_sales[index]
        )
        result_dict["Date"] = sales_dates[index]
        result_list.append(result_dict)
    df_result = pd.DataFrame(result_list)
    print(df_result)

    return result_list


def index(request):
    form = InputForm()
    if request.method == "POST":
        form = InputForm(request.POST)
        context = {
            "form": form
        }
        # commit
        if form.is_valid():
            lst = predict(form.cleaned_data['date'], form.cleaned_data['prev_sales'], form.cleaned_data['sales'])
            context.update(lst[0])
            print(lst[0]['pred_value'], lst[0]['Date'])

        return render(request, 'panel.html', context)
    else:
        return render(request, 'panel.html', {'form': form})
